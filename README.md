## full_k63v2_64_bsp-user 8.1.0 O11019 1544608098 release-keys
- Manufacturer: ulefone
- Platform: mt6763
- Codename: Power_5
- Brand: Ulefone
- Flavor: full_k63v2_64_bsp-user
- Release Version: 8.1.0
8.1.0
- Id: O11019
- Incremental: 1530704333
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Ulefone/Power_5/Power_5:8.1.0/O11019/1530704333:user/release-keys
- OTA version: 
- Branch: full_k63v2_64_bsp-user-8.1.0-O11019-1544608098-release-keys
- Repo: ulefone_power_5_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
