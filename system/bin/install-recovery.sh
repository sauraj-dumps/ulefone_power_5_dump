#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:16006048:b4fa7f969eec68f17ec8494045a776bab333098b; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot:10036128:eea8005e0d1ad0cf4cb1d5c6ccb16d4591013b4a EMMC:/dev/block/platform/bootdevice/by-name/recovery b4fa7f969eec68f17ec8494045a776bab333098b 16006048 eea8005e0d1ad0cf4cb1d5c6ccb16d4591013b4a:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
